import re
import config
from emotionanalysis import EmotionAnalyse
from nltk.corpus import stopwords
tokens_re = re.compile(r'('+'|'.join(config.regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+config.emoticons_str+'$', re.VERBOSE | re.IGNORECASE)
stop = re.compile(r'('+'|'.join(config.regex_str[1:6])+')', re.VERBOSE | re.IGNORECASE)
cachedStopWords = set(stopwords.words("english"))
"""cachedStopWords.update(('a', 'an', 'the', '&', 'and', 'this', 'when', 'it','am', 'is',' are', 'have', '-','_', 'has', 'had',
                     '/', '(', ')', '?', '.', '!', '?', '&', '%',':'
                        , 'cant', 'yes', 'no', 'these', 'are', 'was', 'were', '#', '=', '$', '*'))"""


class PreProcessing:
    def __init__(self, list):
        self.tweet = list
        self.result = []


    def tokenize(self, w):
        return tokens_re.findall(w)

    def pre_process(self):
        token = self.tokenize(self.tweet)
        tokens = [token if emoticon_re.search(token) else token.lower() for token in token]
        self.remove_stop_word(tokens)

    def remove_stop_word(self, words):
        useful_words = [word for word in words if word not in cachedStopWords]
        self.remove_stop(useful_words)

    def remove_stop(self, l):
        remain_word = [word for word in l if not re.search(stop, word)]
        print(remain_word)
        EM = EmotionAnalyse(remain_word)
        semi_final = EM.replace_emotion()
        self.replace_duplicated(semi_final)


    def replace_duplicated(self,w):
        self.result = sorted(set(w), key=lambda x: w.index(x))

    def process(self):
        self.pre_process()
        return self.result


if __name__ == '__main__':
    print("initializing application...")
    list = "hello hugly :)  :/ :(  sad"
    p = PreProcessing(list)
    print(p.process())

    #print(pre)


