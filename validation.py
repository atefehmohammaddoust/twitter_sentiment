from postagging import Tag
def is_adjective(tag):
    if tag == 'JJ' or tag == 'JJR' or tag == 'JJS':
        return True
    else:
        return False


def is_adverb(tag):
    if tag == 'RB' or tag == 'RBR' or tag == 'RBS':
        return True
    else:
        return False


def is_noun(tag):
    if tag == 'NN' or tag == 'NNS' or tag == 'NNP' or tag == 'NNPS':
        return True
    else:
        return False


def is_verb(tag):
    if tag == 'VB' or tag == 'VBD' or tag == 'VBG' or tag == 'VBN' or tag == 'VBP' or tag == 'VBZ':
        return True
    else:
        return False

def is_valid(token):
    if is_noun(token[1]) or is_adverb(token[1]) or is_verb(token[1]) or is_adjective(token[1]):
        return True
    else:
        return False


class Validation:
    def __init__(self, tweet):
        self.tweet = tweet

    def filter_tweet(self):
        return list(map(lambda x: x[0], filter(lambda token: is_valid(token), self.tweet)))


if __name__ == '__main__':
    print("initializing application...")
    tag = Tag(['i', 'ali', 'honey'])
    #print(tag.result[0][1])
    valid = Validation(tag.pos_tagging())
    print(valid.filter_tweet())
