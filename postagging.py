from nltk import pos_tag


class Tag:
    def __init__(self, token):
        self.token = token

    def pos_tagging(self):
        return pos_tag(self.token)


if __name__ == '__main__':
    print("initializing application...")
    list = ['angry', 'the' , 'rt']
    tag = Tag(list)
    print(tag.pos_tagging())