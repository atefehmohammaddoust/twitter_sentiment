class struct:
    import pprint

    pp = pprint.PrettyPrinter(indent=4)
    # statements
    #print("starting application ...")
    # data
    # path = 'test.txt'
    data = []
    person = []
    time = []
    tweet = []
    RTcount = 0
    FAVcount = 0

    def __init__(self, name):
        self.name = name
    # read

    def process(self):

        with open(self.name, 'rt') as f:
            self. data += f.readlines()

        self.data = self.data[1:]
        step = 1
        for item in self.data:
            if step is 1:
                self.time += [item]
            elif step is 2:
                self.person += [item[21:]]
            elif step is 3:
                self.tweet += [item]
            if step is 4:
                step = 1
            else:
                step += 1

        person = list(map(lambda x: x[:len(x)-1], self.person))
        time = list(map(lambda x: x[2:len(x)-1], self.time))
        tweet = list(map(lambda x: x[2:len(x) - 1], self.tweet))
        data = []
        for p, d, t in zip(person, time, tweet):
            if " RT " in t:
                self.RTcount += 1
            if " @ " in t:
                self.FAVcount += 1

            data += [{"person": p, "time": d, "tweet": t}]
            # print(data)
        unique = []
        [unique.append(item) for item in person if item not in unique]
        #print(data)
        return (data)