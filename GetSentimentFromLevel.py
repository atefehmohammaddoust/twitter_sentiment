from validation import Validation
from postagging import Tag
from nltk.corpus import sentiwordnet as swn
from functools import reduce
from negation import NegatingWordReader
from modifier import ModifierWordReader


nwr = NegatingWordReader('NegatingWordList.txt')
mwr = ModifierWordReader('BoosterWordList.txt')

def get_first_synset(word):
    synsets = list(swn.senti_synsets(word))
    if len(synsets) > 0:
        return synsets[0]
    else:
        return None


def get_synsets(tweet):
    return list(filter(lambda x: x is not None, list(map(lambda x: get_first_synset(x), tweet))))


def tweet_has_negation(tweet):
    dweebs = list(map(lambda x: nwr.has_negation(x), tweet))
    return reduce(lambda a, x: a or x, dweebs)


def find_negated_word_index(tweet):
    for i, word in enumerate(tweet):
        if nwr.has_negation(word):
            return i
        else:
            return -1


def find_previous_word_from_tweet(tweet, word):
    word = word[:len(word)-5]
  #  tweet = list(map(lambda x: x[0], tweet))
    try:
        index = tweet.index(word)
        print("the word is ", index)
        if index > 0:
            print("previous word  of {} is ".format(word), tweet[index-1])
            return tweet[index - 1]
        else:
            return None
    except ValueError:
        return None


def get_negScore_from_synsets(sentisynsets, tweet):
    print(sentisynsets)
    tweet_has_negation_word = tweet_has_negation(tweet)
    if tweet_has_negation_word:
        idx = find_negated_word_index(tweet)
        senti_syn = get_first_synset(tweet[idx])
        if senti_syn:
            return senti_syn.pos_score()
        else:

            return 0

    else:
        negScore = 0
        for sentisynset in sentisynsets:
            word = sentisynset.synset.name()
            previous_word = find_previous_word_from_tweet(tweet, word)
           # print(previous_word)
            if mwr.has_modifier(previous_word):
                negScore = negScore + 2 * sentisynset.neg_score()
            else:
                negScore = negScore + sentisynset.neg_score()

        return negScore


def get_posScore_from_synsets(sentisynsets, tweet):
    tweet_has_negation_word = tweet_has_negation(tweet)
    if tweet_has_negation_word:
        idx = find_negated_word_index(tweet)
        senti_syn = get_first_synset(tweet[idx])
        if senti_syn:
            return senti_syn.neg_score()
        else:
            return 0


    else:
        posScore = 0
        print(sentisynsets)
        for sentisynset in sentisynsets:
            word = sentisynset.synset.name()
            previous_word = find_previous_word_from_tweet(tweet, word)
            if mwr.has_modifier(previous_word):
                posScore = posScore + 2 * sentisynset.pos_score()
            else:
                posScore = posScore + sentisynset.pos_score()

        return posScore


def emojiScore(posScore, negScore, tweet):
    for word in tweet:
        if word == "sad":
            negScore += 1
            return negScore
        if word == "happy" or word == "joke":
            posScore += 1
            return posScore


def get_tweet_sentiment_from_score(posScore, negScore):
    if posScore > negScore:
        return 'Positive'
    elif posScore == negScore:
        return 'Neutral'
    else:
        return 'Negative'


def get_sentiment_from_level(i):
    if i == 4:
        return 'Positive'
    elif i == 2:
        return 'Neutral'
    else:
        return 'Negative'


class Sentiment:
    def __init__(self, tweet):
        self.tweet = tweet
        self.result = {}
        self.get_sentiment_from_tweet()


    def get_sentiment_from_tweet(self):
        sentisynsets = get_synsets(self.tweet)
        posScore = get_posScore_from_synsets(sentisynsets, self.tweet)
        negScore = get_negScore_from_synsets(sentisynsets, self.tweet)
        emojiScore(negScore, posScore, self.tweet)
        print("final result positive score is {} AND negative score is {} ".format(posScore, negScore))
        sentiment = get_tweet_sentiment_from_score(posScore, negScore)
        self.result = {"posScore": posScore, "negScore": negScore, "sentiment": sentiment}
        return self.result



if __name__ == '__main__':
    print("initializing application...")
    tag = Tag(['verify', 'the', 'happy', 'love', 'is ', 'honey'])
    sid = Validation(tag.result)
    senti = Sentiment(sid.result)
    #print(senti.result)