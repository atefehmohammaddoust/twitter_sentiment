import sqlite3
from tqdm import tqdm
conn = sqlite3.connect('twitter.db')
cur = conn.cursor()


class ReadFromDb:
    def __init__(self, table):
        self.table = table
        self.result = []
        self.row = {}
        self.read_table()

    def read_table(self):
        #length = cur.execute("select count(*) from  %s " % self.table).fetchall()
        self.row = cur.execute("SELECT tweet from  %s " % self.table).fetchall()
        return self.row


if __name__ == '__main__':
    print("initializing application...")
    R = ReadFromDb('TEST')
    print(R.row)
