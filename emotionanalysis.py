import re
import config
emoticon_re = re.compile(r'^'+config.emoticons_str+'$', re.VERBOSE | re.IGNORECASE)
sad_RE = re.compile(r'^'+config.emotion[1]+'$', re.VERBOSE | re.IGNORECASE)
joke_RE = re.compile(r'^'+config.emotion[2]+'$', re.VERBOSE | re.IGNORECASE)
happy_RE = re.compile(r'^'+config.emotion[0]+'$', re.VERBOSE | re.IGNORECASE)


class EmotionAnalyse:
    def __init__(self, list):
        self.list = list

    def analyse_emo(self, word):
        try:
            happy = re.search(happy_RE, word)
            sad = re.search(sad_RE, word)
            joke = re.search(joke_RE, word)
            if happy:
                return "happy"
            if sad:
                return "sad"
            if joke:
                return "joke"
        except BaseException as err:
            print(err)

    def replace_emotion(self):
            final_result = [self.analyse_emo(word) if re.search(emoticon_re, word) else word for word in self.list]
            #print(final_result)
            return final_result


        #replace_duplicated(final_result)

if __name__ == '__main__':
    print("initializing application...")
    l = [':)', ':(', 'hello']
    ea = EmotionAnalyse(l)


